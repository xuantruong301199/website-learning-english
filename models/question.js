const ObjectID = require('mongoose').Types.ObjectId;
const QUESTION_COLL = require('../databases/question-coll');
const EXAM_COLL = require('../databases/exam-coll');

module.exports = class question extends QUESTION_COLL{
    static insert({name, image, answer, examID, userID}){
        return new Promise(async resolve =>{
            try {
                let dataInsert={ name, image, answer, examID, userID }
                
                if(!ObjectID.isValid(examID) || !ObjectID.isValid(userID))
                    return resolve({error: true, message: 'khong hop le'});
                
                let checkExistExamID = await EXAM_COLL.findById(examID)
                if(!checkExistExamID)
                    return resolve({error: true, message: 'Khong ton tai'});
                
                if(examID && ObjectID.isValid(examID)){
                    dataInsert.exam = examID;
                }

                if(image){
                    dataInsert.image = image;
                }
                console.log(name, image, answer, examID, userID );

                let infoAfterInsert = new QUESTION_COLL(dataInsert);
                let saveDataInsert = await infoAfterInsert.save();
                console.log({ saveDataInsert });
                
                if(!saveDataInsert)
                    return resolve({ error: true, message: "cannot_insert"});
                return resolve({ error: false, data: infoAfterInsert});
                // let arrayExamHaveQuestion = await EXAM_COLL.findByIdAndUpdate(examID, 
                //     {
                //         $push: { question: infoAfterInsert._id }
                //     }, {new: true})
            } catch (error) {
                return resolve({ error: true , message: error.message});
            }
        });
    }
}
