const ObjectID =require('mongoose').Types.ObjectId;
const EXAM_COLL = require('../databases/exam-coll');

module.exports = class exam extends EXAM_COLL{

    static insert({ name}){
        return new Promise(async resolve => {
            try{
                if(!name)
                return resolve({ error: true, message:'khong hop le'});

                let dataInsert= {
                    name
                };

                let infoAfterInsert = new EXAM_COLL(dataInsert);
                let saveAfterInsert = await infoAfterInsert.save();
                if(!saveAfterInsert) 
                return resolve({error: true, messsage: 'Khong the them'});
                resolve({error:false , message: infoAfterInsert});

            
            }catch (error){
                return resolve({error:true, message: error.message});
            }
        });
    }
    static 
}
