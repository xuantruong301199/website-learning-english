const route  = require('express').Router();
const EXAM_MODEL = require('../models/exam');

route.post('/add-exam', async(req, res) =>{
    let{name}= req.body;
    let infoAfterInsert = await EXAM_MODEL.insert({name});
    res.json(infoAfterInsert)
});

module.exports = route;