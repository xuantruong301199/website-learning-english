const route = require('express').Router();
const QUESTION_MODEL = require('../models/question');

route.post('/add-question', async (req, res) => {
    try{
        let{ name, image, answer, examID, userID } = req.body;
        //console.log(name, image, answer, examID, userID );
        let infoQuestion = await QUESTION_MODEL.insert({ name, image, answer, examID, userID });
        console.log({ infoQuestion });
        return res.json(infoQuestion);

    } catch(error) {
        res.json(error.message);
    }
    
});
module.exports = route;

